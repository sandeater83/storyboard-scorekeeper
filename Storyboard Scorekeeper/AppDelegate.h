//
//  AppDelegate.h
//  Storyboard Scorekeeper
//
//  Created by Stuart Adams on 2/27/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

