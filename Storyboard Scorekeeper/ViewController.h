//
//  ViewController.h
//  Storyboard Scorekeeper
//
//  Created by Stuart Adams on 2/27/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel* playerOneScore;
@property (strong, nonatomic) IBOutlet UILabel* playerTwoScore;

@property (strong, nonatomic) IBOutlet UIStepper* playerOneStepper;
@property (strong, nonatomic) IBOutlet UIStepper* playerTwoStepper;

@property (strong, nonatomic) IBOutlet UIButton* resetScore;

-(IBAction)scoreChanged:(id)sender;
-(IBAction)resetScore:(id)sender;


@end

