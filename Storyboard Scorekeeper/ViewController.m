//
//  ViewController.m
//  Storyboard Scorekeeper
//
//  Created by Stuart Adams on 2/27/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _playerOneStepper.tag = 1;
    _playerTwoStepper.tag = 2;
    
    _playerOneScore.font = [UIFont systemFontOfSize:50];
    _playerTwoScore.font = [UIFont systemFontOfSize:50];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)scoreChanged:(id)sender
{
    if([sender isKindOfClass:[UIStepper class]])
    {
        UIStepper* stepper = (UIStepper*) sender;
        switch (stepper.tag) {
            case 1:
                _playerOneScore.text = [NSString stringWithFormat:@"%.0f",stepper.value];
                break;
            case 2:
                _playerTwoScore.text = [NSString stringWithFormat:@"%.0f",stepper.value];
                break;
            default:
                break;
        }
    }
}

-(IBAction)resetScore:(id)sender
{
    _playerOneScore.text = @"0";
    _playerTwoScore.text = @"0";
}

@end
